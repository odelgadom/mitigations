# Mitigations

Create a list of current Mitre ATTC&K mitigations and the corresponding techniques addressed by each one.
Mitigations listed by the script has been verified versus https://collaborate.mitre.org/attackics/index.php/Mitigations and seem to match well. However, there is no exact information on how often the two are syncronized.
