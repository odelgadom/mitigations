#!/usr/bin/env python3

##########
# Name:         MITRE ATT&CK Mitigations
# Purpose:      Create a list of current mitigations and the corresponding techniques addressed
# Contact:      Oscar J. Delgado-Melo (@odelgado)
# 
# NOTE: It is required to install attackcti -> pip install attackcti
#       https://pypi.org/project/attackcti/
##########
import argparse 
from attackcti import attack_client
import json
import sys

# parse arguments
ap = argparse.ArgumentParser()
group = ap.add_mutually_exclusive_group(required=True)
group.add_argument('-i', '--ics', action='store_true',  help='ICS version of the Mitre ATT&CK Matrix')
group.add_argument('-e', '--ent', action='store_true',  help='Enterprise version of the Mitre ATT&CK Matrix')
group.add_argument('-a', '--all', action='store_true',  help='Mobile, Enterprise and ICS version of the Mitre ATT&CK Matrix')
args = vars(ap.parse_args())

if  len(sys.argv) == 1:
    ap.print_help()
    sys.exit()

ics = args["ics"]
ent = args["ent"]
all = args["all"]

lift = attack_client()
if ics: 
    tecniques = lift.get_ics_techniques()
    mitigations = lift.get_ics_mitigations()
    relationships = lift.get_ics_relationships()
    version = "ICS"
elif ent:
    tecniques = lift.get_enterprise_techniques()
    mitigations = lift.get_enterprise_mitigations()
    relationships = lift.get_enterprise_relationships()
    version = "Enterprise"
else:
    tecniques = lift.get_techniques()
    mitigations = lift.get_mitigations()
    relationships = lift.get_relationships()
    version = "All"

print("------------------------")
print("MITRE ATT&CK Mitigations")
print("------------------------\n")
print(f"{version} version selected")
print(f"Mitigations Count: {len(mitigations)}")
print(f"Techniques Count: {len(tecniques)}")
print()

# create a dictionary based on technique ID
techDict={}
for tq in tecniques:
    techDict[tq["id"]]=tq["external_references"][0]["external_id"]

# there are thousands of relationships, but the ones relating techniques to mitigations use the keyword "mitigates", 
mitigationsR = []
for mt in mitigations:
    lst=[]
    for rt in relationships:
        if (rt["relationship_type"] == "mitigates"):
            if rt["source_ref"] == mt["id"]:
                lst.append(techDict[rt["target_ref"]])
    lst.sort()
    # organize data for JSON file
    dct ={}
    dct["id"] = mt["external_references"][0]["external_id"]
    dct["name"] = mt["name"]
    dct["description"] = mt["description"]
    dct["ttps"] = lst
    mitigationsR.append(dct)

mitigationsR.sort(key=lambda x:x['id'])

# encode de JSON file
with open(f"{version}_Mitigation.json", "w") as write_file:
    json.dump({"mitigations":mitigationsR}, write_file, indent=2)

print(f"Dump completed. Check \"{version}_Mitigation.json\"")